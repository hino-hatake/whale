FROM nginx:alpine

LABEL maintainer="Hino <sinhngay3110@gmail.com>"

RUN rm -r /usr/share/nginx/html
COPY html /usr/share/nginx/html

RUN rm /etc/nginx/conf.d/*
COPY nginx.conf /etc/nginx/conf.d/
